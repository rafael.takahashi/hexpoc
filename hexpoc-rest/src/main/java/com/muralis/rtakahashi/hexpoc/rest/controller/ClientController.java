package com.muralis.rtakahashi.hexpoc.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.muralis.rtakahashi.hexpoc.core.navigation.INavigationRunner;
import com.muralis.rtakahashi.hexpoc.core.navigation.Navigation;
import com.muralis.rtakahashi.hexpoc.domain.Client;
import com.muralis.rtakahashi.hexpoc.rest.dto.ClientDTO;

@RestController
public class ClientController {
	
	@Autowired
	@Qualifier("navigationRunner")
	INavigationRunner<Client> navigationRunner;
	
	@Autowired
	@Qualifier("INSERT_CLIENT")
	Navigation<Client> insertCase;
	
	@Autowired
	@Qualifier("GET_ONE_CLIENT")
	Navigation<Client> getCase;
	
	@Autowired
	@Qualifier("GET_LIST_CLIENTS")
	Navigation<Client> listCase;
	
	@RequestMapping(method = RequestMethod.GET, value = "/clients")
	public @ResponseBody List<ClientDTO> getClients() {
		// "clearing" the context is definitely wrong, we should be
		// creating a new navigation case every time. But this will
		// do for now.
		listCase.clear();
		navigationRunner.run(null, listCase);
		List<Client> clients = listCase.getContext().getValue("result");
		List<ClientDTO> convertedList = new ArrayList<>();
		for (int i = 0; i < clients.size(); i++) {
			convertedList.add(new ClientDTO(clients.get(i)));
		}
		return convertedList;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/clients")
	public @ResponseBody ClientDTO addClient(@RequestBody ClientDTO client) {
		insertCase.clear();
		navigationRunner.run(client.toDomainObject(), insertCase);
		Client result = insertCase.getContext().getValue("result");
		return new ClientDTO(result);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/clients/{id}")
	public @ResponseBody ClientDTO getClient(@PathVariable String id) {
		getCase.clear();
		getCase.getContext().setValue("id", id);
		Client result = getCase.getContext().getValue("result");
		return new ClientDTO(result);
	}
}
