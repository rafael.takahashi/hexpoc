package com.muralis.rtakahashi.hexpoc.rest.dto;

import java.time.LocalDate;

import com.muralis.rtakahashi.hexpoc.domain.Client;

public class ClientDTO {
	public ClientDTO() { }
	
	public ClientDTO(Client client) {
		this.setId(client.getId());
		this.setName(new NameDTO(client.getName()));
		this.setBirthDate(client.getBirthDate());
	}
	
	public Client toDomainObject() {
		Client client = new Client();
		client.setId(this.id);
		client.setName(this.name.toDomainObject());
		client.setBirthDate(this.birthDate);
		return client;
	}
	
	private String id;
	private NameDTO name;
	private LocalDate birthDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public NameDTO getName() {
		return name;
	}
	public void setName(NameDTO name) {
		this.name = name;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
}
