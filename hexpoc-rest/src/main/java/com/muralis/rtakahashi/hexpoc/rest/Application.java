package com.muralis.rtakahashi.hexpoc.rest;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import feign.codec.Decoder;
import feign.jackson.JacksonDecoder;

@SpringBootApplication
@ComponentScan(basePackages = {"com.muralis.rtakahashi.hexpoc"})
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
	}
}
