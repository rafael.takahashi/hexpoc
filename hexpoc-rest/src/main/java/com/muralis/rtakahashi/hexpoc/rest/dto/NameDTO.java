package com.muralis.rtakahashi.hexpoc.rest.dto;

import com.muralis.rtakahashi.hexpoc.domain.ChineseName;
import com.muralis.rtakahashi.hexpoc.domain.EnglishName;
import com.muralis.rtakahashi.hexpoc.domain.HumanName;
import com.muralis.rtakahashi.hexpoc.domain.IcelandicName;
import com.muralis.rtakahashi.hexpoc.domain.JapaneseName;

/**
 * Object that represents input/output of this rest service.
 * This is a union type, where the fields depend of the value
 * of "type".
 * 
 * Definitely doesn't look like we should be doing conversions
 * here. Maybe a factory would be more appropriate?
 */
public class NameDTO {
	public NameDTO() { }
	
	public NameDTO(HumanName name) {
		if (name instanceof EnglishName) {
			EnglishName englishName = (EnglishName)name;
			this.type = "ENGLISH";
			this.firstName = englishName.getFirstName();
			this.middleName = englishName.getMiddleName();
			this.lastName = englishName.getLastName();
		} else if (name instanceof IcelandicName) {
			IcelandicName icelandicName = (IcelandicName)name;
			this.type = "ICELANDIC";
			this.givenName = icelandicName.getGivenName();
			this.patronymic = icelandicName.getPatronymic();
			this.familyName = icelandicName.getFamilyName();
		} else if (name instanceof ChineseName) {
			ChineseName chineseName = (ChineseName)name;
			this.familyName = chineseName.getFamilyName();
			this.generationalName = chineseName.getGenerationalName();
			this.givenName = chineseName.getGivenName();
		} else if (name instanceof JapaneseName) {
			JapaneseName japaneseName = (JapaneseName)name;
			this.familyName = japaneseName.getFamilyName();
			this.givenName = japaneseName.getGivenName();
			this.familyNameReading = japaneseName.getFamilyNameReading();
			this.givenNameReading = japaneseName.getGivenNameReading();
		} else {
			throw new IllegalArgumentException("Name cannot be converted to DTO.");
		}
	}
	
	public HumanName toDomainObject() {
		switch (this.type.toUpperCase()) {
		case "ENGLISH":
			EnglishName englishName = new EnglishName();
			englishName.setFirstName(this.firstName);
			englishName.setMiddleName(this.middleName);
			englishName.setLastName(this.lastName);
			return englishName;
		case "ICELANDIC":
			IcelandicName icelandicName = new IcelandicName();
			icelandicName.setFamilyName(this.familyName);
			icelandicName.setPatronymic(this.patronymic);
			icelandicName.setGivenName(this.givenName);
			return icelandicName;
		case "CHINESE":
			ChineseName chineseName = new ChineseName();
			chineseName.setFamilyName(this.familyName);
			chineseName.setGenerationalName(this.generationalName);
			chineseName.setGivenName(this.givenName);
			return chineseName;
		case "JAPANESE":
			JapaneseName japaneseName = new JapaneseName();
			japaneseName.setFamilyName(this.familyName);
			japaneseName.setFamilyNameReading(this.familyNameReading);
			japaneseName.setGivenName(this.givenName);
			japaneseName.setGivenNameReading(this.givenNameReading);
			return japaneseName;
		default:
			throw new IllegalArgumentException("Unrecognized name type " + this.type + ".");
		}
	}
	
	private String type;
	private String familyName;
	private String generationalName;
	private String patronymic;
	private String givenName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String familyNameReading;
	private String givenNameReading;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGenerationalName() {
		return generationalName;
	}
	public void setGenerationalName(String generationalName) {
		this.generationalName = generationalName;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFamilyNameReading() {
		return familyNameReading;
	}
	public void setFamilyNameReading(String familyNameReading) {
		this.familyNameReading = familyNameReading;
	}
	public String getGivenNameReading() {
		return givenNameReading;
	}
	public void setGivenNameReading(String givenNameReading) {
		this.givenNameReading = givenNameReading;
	}
}
