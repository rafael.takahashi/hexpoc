package com.muralis.rtakahashi.hexpoc.core.navigation;

public interface IStrategy<T> {
	public void process(T entity, NavigationContext context);
}
