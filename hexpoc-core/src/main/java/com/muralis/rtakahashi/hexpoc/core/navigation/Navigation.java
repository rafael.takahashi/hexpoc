package com.muralis.rtakahashi.hexpoc.core.navigation;

import java.util.ArrayList;
import java.util.List;

public class Navigation<T> {
	public Navigation() {
		this.strategies = new ArrayList<>();
		this.context = new NavigationContext();
	};
	
	@SafeVarargs
	public static <T> Navigation<T> from(IStrategy<T>... strategies) {
		Navigation<T> nav = new Navigation<>();
		for (int i = 0; i < strategies.length; i++) {
			nav.getStrategies().add(strategies[i]);
		}
		return nav;
	}
	
	public void clear() {
		this.context = new NavigationContext();
	}
	
	private List<IStrategy<T>> strategies;
	
	private NavigationContext context;

	public List<IStrategy<T>> getStrategies() {
		return strategies;
	}

	public NavigationContext getContext() {
		return context;
	}

}
