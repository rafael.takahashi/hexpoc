package com.muralis.rtakahashi.hexpoc.core.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.muralis.rtakahashi.hexpoc.core.navigation.IStrategy;
import com.muralis.rtakahashi.hexpoc.core.navigation.NavigationContext;
import com.muralis.rtakahashi.hexpoc.domain.Client;

@Component
public class ValidateBirthDate implements IStrategy<Client> {
	@Override
	public void process(Client client, NavigationContext context) {
		if (client.getBirthDate().isAfter(LocalDate.now())) {
			context.addError("Birth date cannot be after now.");
			return;
		}
	}
}
