package com.muralis.rtakahashi.hexpoc.core.impl;

import org.springframework.stereotype.Component;

import com.muralis.rtakahashi.hexpoc.domain.Client;

import com.muralis.rtakahashi.hexpoc.core.navigation.IStrategy;
import com.muralis.rtakahashi.hexpoc.core.navigation.NavigationContext;

@Component
public class ValidatePermission implements IStrategy<Client> {

	@Override
	public void process(Client entity, NavigationContext context) {
		// Pretend we're doing some computation to check if the user
		// has the necessary permissions.
		// This POC doesn't have any authentication configured, so
		// we can't do anything.
	}

}
