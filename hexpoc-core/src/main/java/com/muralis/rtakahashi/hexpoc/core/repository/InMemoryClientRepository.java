package com.muralis.rtakahashi.hexpoc.core.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.muralis.rtakahashi.hexpoc.domain.Client;

/**
 * In-memory mock implementation of Repository.
 *
 */
@Component
public class InMemoryClientRepository implements Repository<Client> {
	
	public InMemoryClientRepository() {
		this.clients = new ArrayList<>();
	}
	
	private List<Client> clients;
	private Integer nextId = 1;

	@Override
	public List<Client> getList() {
		ArrayList<Client> array = new ArrayList<Client>();
		array.addAll(this.clients);
		return array;
	}

	@Override
	public Client getOne(String id) {
		for (int i = 0; i < this.clients.size(); i++) {
			if (this.clients.get(i).getId().equals(id)) {
				return this.clients.get(i);
			}
		}
		return null;
	}

	@Override
	public Client insert(Client client) {
		// Generate new id
		client.setId(this.nextId.toString());
		this.nextId++;
		
		this.clients.add(client);
		
		return client;
	}

}
