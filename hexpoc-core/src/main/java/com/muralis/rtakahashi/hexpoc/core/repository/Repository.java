package com.muralis.rtakahashi.hexpoc.core.repository;

import java.util.List;

public interface Repository<T> {
	public List<T> getList();
	
	public T getOne(String id);
	
	public T insert(T t);
}
