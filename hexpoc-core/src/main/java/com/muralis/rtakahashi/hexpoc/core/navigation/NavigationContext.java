package com.muralis.rtakahashi.hexpoc.core.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavigationContext {
	public NavigationContext() {
		this.map = new HashMap<>();
		this.errors = new ArrayList<>();
	}
	
	private Map<String, Object> map;
	
	private List<String> errors;
	
	public void setValue(String key, Object value) {
		this.map.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		return (T) this.map.get(key);
	}
	
	public void addError(String error) {
		this.errors.add(error);
	}
	
	public boolean hasError() {
		return !this.errors.isEmpty();
	}
}
