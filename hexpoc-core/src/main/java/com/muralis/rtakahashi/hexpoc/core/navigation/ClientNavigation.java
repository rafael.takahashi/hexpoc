package com.muralis.rtakahashi.hexpoc.core.navigation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.muralis.rtakahashi.hexpoc.core.impl.FetchListClient;
import com.muralis.rtakahashi.hexpoc.core.impl.FetchOneClient;
import com.muralis.rtakahashi.hexpoc.core.impl.InsertClient;
import com.muralis.rtakahashi.hexpoc.core.impl.ValidateBirthDate;
import com.muralis.rtakahashi.hexpoc.core.impl.ValidatePermission;
import com.muralis.rtakahashi.hexpoc.domain.Client;

@Configuration
public class ClientNavigation {
	
	@Autowired
	FetchListClient fetchListClient;

	@Autowired
	FetchOneClient fetchOneClient;

	@Autowired
	InsertClient insertClient;

	@Autowired
	ValidateBirthDate validateBirthDate;

	@Autowired
	ValidatePermission validatePermission;
	
	@Bean(name = "INSERT_CLIENT")
	public Navigation<Client> insert() {
		return Navigation.from(validatePermission, validateBirthDate, insertClient);
	}
	
	@Bean(name = "GET_ONE_CLIENT")
	public Navigation<Client> getOne() {
		return Navigation.from(validatePermission, fetchOneClient);
	}
	
	@Bean(name = "GET_LIST_CLIENTS")
	public Navigation<Client> getList() {
		return Navigation.from(validatePermission, fetchListClient);
	}
}
