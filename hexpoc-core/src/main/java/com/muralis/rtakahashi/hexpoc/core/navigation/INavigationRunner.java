package com.muralis.rtakahashi.hexpoc.core.navigation;

// Currently our navigator doesn't do anything special.
public interface INavigationRunner<T> {
	public void run(T entity, Navigation<T> nav);
}
