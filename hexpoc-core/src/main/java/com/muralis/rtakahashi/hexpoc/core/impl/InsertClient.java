package com.muralis.rtakahashi.hexpoc.core.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.muralis.rtakahashi.hexpoc.core.navigation.IStrategy;
import com.muralis.rtakahashi.hexpoc.core.navigation.NavigationContext;
import com.muralis.rtakahashi.hexpoc.core.repository.Repository;
import com.muralis.rtakahashi.hexpoc.domain.Client;

@Configuration
public class InsertClient implements IStrategy<Client> {
	
	@Autowired
	Repository<Client> repository;

	@Override
	public void process(Client entity, NavigationContext context) {
		// In a real project there would be error handling here.
		Client result = repository.insert(entity);
		context.setValue("result", result);
	}
	
}
