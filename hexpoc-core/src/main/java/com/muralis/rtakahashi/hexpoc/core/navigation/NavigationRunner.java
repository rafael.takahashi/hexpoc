package com.muralis.rtakahashi.hexpoc.core.navigation;

import org.springframework.stereotype.Component;

/**
 * Super simple navigator that doesn't do anything special.
 */
@Component("navigationRunner")
public class NavigationRunner<T> implements INavigationRunner<T> {

	@Override
	public void run(T entity, Navigation<T> nav) {
		for (int i = 0; i < nav.getStrategies().size(); i++) {
			IStrategy<T> strategy = nav.getStrategies().get(i);
			strategy.process(entity, nav.getContext());
			if (nav.getContext().hasError()) {
				break;
			}
		}
	}
}
