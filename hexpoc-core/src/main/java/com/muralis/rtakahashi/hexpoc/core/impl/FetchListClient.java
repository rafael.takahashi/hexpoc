package com.muralis.rtakahashi.hexpoc.core.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.muralis.rtakahashi.hexpoc.core.navigation.IStrategy;
import com.muralis.rtakahashi.hexpoc.core.navigation.NavigationContext;
import com.muralis.rtakahashi.hexpoc.core.repository.Repository;
import com.muralis.rtakahashi.hexpoc.domain.Client;

@Configuration
public class FetchListClient implements IStrategy<Client> {

	@Autowired
	Repository<Client> repository;

	@Override
	public void process(Client entity, NavigationContext context) {
		// In a real project there would be error handling here.
		List<Client> clients = repository.getList();
		context.setValue("result", clients);
	}
}
