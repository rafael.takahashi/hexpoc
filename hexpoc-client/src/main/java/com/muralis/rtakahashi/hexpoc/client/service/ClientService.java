package com.muralis.rtakahashi.hexpoc.client.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muralis.rtakahashi.hexpoc.rest.dto.ClientDTO;


@FeignClient(name="clients", url="http://localhost:8080")
public interface ClientService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/clients")
	List<ClientDTO> getClients();
	
	@RequestMapping(method = RequestMethod.POST, value = "/clients")
	@ResponseBody ClientDTO addClient(@RequestBody ClientDTO client);
	
	@RequestMapping(method = RequestMethod.GET, value="/clients/{id}")
	@ResponseBody ClientDTO getClient(@PathVariable("id") String id);
}
