package com.muralis.rtakahashi.hexpoc.client;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.muralis.rtakahashi.hexpoc.client.service.ClientService;
import com.muralis.rtakahashi.hexpoc.rest.dto.ClientDTO;
import com.muralis.rtakahashi.hexpoc.rest.dto.NameDTO;

@SpringBootApplication
@EnableFeignClients
public class Application {
	
	@Autowired
	ClientService clientService;
	
	public static void main(String[] args) {
		// This runs as if it were a server, but I'm only
		// using it as a test.
		SpringApplication.run(Application.class, args);
	}
	
	private String testPost() {
		NameDTO name1 = new NameDTO();
		name1.setType("ENGLISH");
		name1.setFirstName("Cosmo");
		name1.setLastName("Kramer");
		LocalDate bDate1 = LocalDate.of(1949, 6, 24);
		ClientDTO client1 = new ClientDTO();
		client1.setName(name1);
		client1.setBirthDate(bDate1);
		
		ClientDTO response1 = clientService.addClient(client1);
		if (response1 == null) {
			return "Could not insert client 1";
		} else {
			System.out.println("Client 1 inserted.");
		}
		
		NameDTO name2 = new NameDTO();
		name2.setType("CHINESE");
		name2.setFamilyName("王");
		name2.setGivenName("秀英");
		LocalDate bDate2 = LocalDate.of(1999, 1, 10);
		ClientDTO client2 = new ClientDTO();
		client2.setName(name2);
		client2.setBirthDate(bDate2);
		
		ClientDTO response2 = clientService.addClient(client2);
		if (response2 == null) {
			return "Could not insert client 2";
		} else {
			System.out.println("Client 2 inserted.");
		}
		
		NameDTO name3 = new NameDTO();
		name3.setType("JAPANESE");
		name3.setFamilyName("中本");
		name3.setLastName("聡");
		name3.setFamilyNameReading("なかもと");
		name3.setGivenNameReading("さとし");
		LocalDate bDate3 = LocalDate.of(1964, 3, 15);
		ClientDTO client3 = new ClientDTO();
		client3.setName(name3);
		client3.setBirthDate(bDate3);
		
		ClientDTO response3 = clientService.addClient(client3);
		if (response3 == null) {
			return "Could not insert client 3";
		} else {
			System.out.println("Client 3 inserted.");
		}
		
		return null;
	}
	
	private String testGet() {
		List<ClientDTO> response = clientService.getClients();
		if (response == null) {
			return "Could not read clients";
		} else {
			System.out.println("" + response.size() + " clients read.");
		}
		
		return null;
	}
	
	@PostConstruct
	public void run() {
		String error = null;
		error = this.testPost();
		if (error != null) {
			System.out.println(error);
			return;
		}
		error = this.testGet();
		if (error != null) {
			System.out.println(error);
			return;
		}
	}
}
