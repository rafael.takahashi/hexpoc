package com.muralis.rtakahashi.hexpoc.domain;

/**
 * Ex. 桜井 (さくらい) 繁 (しげる)<br/>
 * 桜井 = Family name<br/>
 * さくらい = Reading of family name<br/>
 * 繁 = Given name<br/>
 * しげる = Reading of given name<br/>
 * In the west, this would be given as "Shigeru Sakurai"
 * 
 * The reading (pronunciation) of Japanese names must always
 * be given, because reading and spelling aren't necessarily connected.
 * 
 * For example, the given name Satoru can be spelled 悟, 聡, 智, 覚, 知, etc.
 * And the given name 大翔 can be pronounced Taisho, Taito, Daito, Haruto,
 * Hiroto, Masato, etc. The reading of a name is given in kana
 * (e.g. "ひかる", not "Hikaru").
 *
 */
public class JapaneseName extends HumanName {
	private String familyName;
	private String givenName;
	private String familyNameReading;
	private String givenNameReading;
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFamilyNameReading() {
		return familyNameReading;
	}
	public void setFamilyNameReading(String familyNameReading) {
		this.familyNameReading = familyNameReading;
	}
	public String getGivenNameReading() {
		return givenNameReading;
	}
	public void setGivenNameReading(String givenNameReading) {
		this.givenNameReading = givenNameReading;
	}
	
	@Override
	public String getAddressableName() {
		// Ex. "桜井". In Japanese localized text, the appropriate
		// honorific should always be added to the name, like
		// "桜井さん" (Sakurai-san). The honorific depends on the
		// context, so it can't be appended here.
		return this.familyName;
	}
	@Override
	public String getFullName() {
		// Example: "桜井繁　(さくらい　しげる)".
		// Note the full-width space characters.
		// We include the reading in parentheses, but that's not always necessary.
		return this.familyName + this.givenName;
	}

}
