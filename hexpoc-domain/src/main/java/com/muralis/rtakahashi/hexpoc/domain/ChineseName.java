package com.muralis.rtakahashi.hexpoc.domain;

/**
 * Example: 李文龍 (Lǐ Wénlóng), or Michael Li <br/>
 * 李 (Lǐ) = Family name <br/>
 * 文 (Wén) = Generational name <br/>
 * 龍 (Lóng) = Given name <br/>
 * Michael = Western name
 */
public class ChineseName extends HumanName {
	private String givenName;
	private String generationalName;
	private String familyName;
	private String westernName;
	
	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getGenerationalName() {
		return generationalName;
	}

	public void setGenerationalName(String generationalName) {
		this.generationalName = generationalName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	@Override
	public String getAddressableName() {
		return this.givenName;
	}
	
	@Override
	public String getFullName() {
		// Ex. Michael 李
		if (westernName != null && !westernName.isEmpty()) {
			return this.westernName + " " + this.givenName + " " + this.familyName;
		}
		
		// Ex. 李文龍 (Generational name is often not present.)
		return this.familyName + this.generationalName + this.givenName;
	}
}
