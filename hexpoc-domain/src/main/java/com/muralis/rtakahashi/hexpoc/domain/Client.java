package com.muralis.rtakahashi.hexpoc.domain;

import java.time.LocalDate;

public class Client {
	private String id;
	private HumanName name;
	private LocalDate birthDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public HumanName getName() {
		return name;
	}
	public void setName(HumanName name) {
		this.name = name;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
}
