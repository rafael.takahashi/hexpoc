package com.muralis.rtakahashi.hexpoc.domain;


/**
 * A person's name.
 * I created multiple implementations for this class
 * because I wanted to think more about mapping this
 * to a data transfer object. The basic idea is that
 * this becomes a union type in JSON.
 *
 */
public abstract class HumanName {
	public abstract String getAddressableName();
	public abstract String getFullName();
}
