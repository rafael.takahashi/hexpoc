package com.muralis.rtakahashi.hexpoc.domain;

/**
 * Traditional nordic name. Nowadays common in Iceland only. <br/>
 * Ex. Jón Þorláksson <br/>
 * Jón = given name <br/>
 * Þorláksson = patronymic, occasionally matronymic <br/>
 * If there's a family name, it comes between the
 * given name and the patronymic.
 */
public class IcelandicName extends HumanName {
	private String givenName;
	private String patronymic;
	private String familyName;
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	
	@Override
	public String getAddressableName() {
		return this.givenName;
	}
	
	@Override
	public String getFullName() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.givenName);
		return this.givenName + " " + this.patronymic;
	}
	
	
}
