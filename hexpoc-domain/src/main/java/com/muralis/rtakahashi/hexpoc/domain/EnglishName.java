package com.muralis.rtakahashi.hexpoc.domain;

public class EnglishName extends HumanName {
	private String firstName;
	private String middleName;
	private String lastName;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String getAddressableName() {
		return this.firstName;
	}
	@Override
	public String getFullName() {
		StringBuilder sb = new StringBuilder();
		sb.append(firstName);
		sb.append(" ");
		if (this.middleName != null && !this.middleName.isEmpty()) {
			sb.append(this.middleName);
			sb.append(" ");
		}
		sb.append(lastName);
		return sb.toString();
	}
}
